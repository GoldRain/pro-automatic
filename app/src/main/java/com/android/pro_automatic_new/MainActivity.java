package com.android.pro_automatic_new;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.pro_automatic_new.sub.EltavlerActivity;
import com.android.pro_automatic_new.sub.KontaktActivity;
import com.android.pro_automatic_new.sub.ProfilActivity;
import com.android.pro_automatic_new.sub.ServiceActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    TextView txv_web, txv_eltavler, txv_service, txv_profil, txv_kontakt;
    ImageView imv_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadLayout();
    }

    private void loadLayout(){

        imv_info = (ImageView) findViewById(R.id.imv_info);
        imv_info.setOnClickListener(this);

        txv_web = (TextView) findViewById(R.id.txv_web);
        txv_web.setOnClickListener(this);

        txv_eltavler = (TextView)findViewById(R.id.txv_eltavler);
        txv_eltavler.setOnClickListener(this);

        txv_service = (TextView) findViewById(R.id.txv_service);
        txv_service.setOnClickListener(this);

        txv_profil = (TextView) findViewById(R.id.txv_profil);
        txv_profil.setOnClickListener(this);

        txv_kontakt = (TextView) findViewById(R.id.txv_kontakt);
        txv_kontakt.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_info:

                startActivity(new Intent(this, InformationActivity.class));
                break;

            case R.id.txv_web:
                startActivity(new Intent(this, WebActivity.class));
                break;

            case R.id.txv_eltavler:
                startActivity(new Intent(this, EltavlerActivity.class));
                break;

            case R.id.txv_service:
                startActivity(new Intent(this, ServiceActivity.class));
                break;

            case R.id.txv_profil:
                startActivity(new Intent(this, ProfilActivity.class));
                break;

            case R.id.txv_kontakt:
                startActivity(new Intent(this, KontaktActivity.class));
                break;

        }
    }
}