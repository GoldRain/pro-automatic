package com.android.pro_automatic_new.sub;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.pro_automatic_new.R;

import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.SmartCallback;

public class ServiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        checkPermission();

        ImageView imv_back = (ImageView) findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(EltavlerActivity.this, MainActivity.class));
                finish();
            }
        });

        TextView txv_phone = (TextView) findViewById(R.id.txv_phone);
        txv_phone.setMovementMethod(LinkMovementMethod.getInstance());
        txv_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callIntent();
            }
        });

        TextView txv_service_hyper = (TextView) findViewById(R.id.txv_service_hyper);
        txv_service_hyper.setMovementMethod(LinkMovementMethod.getInstance());

    }

    private void checkPermission() {

        PermissionManager.Builder()
                .permission(PermissionEnum.CALL_PHONE)
                .askAgain(false)
                .callback(new SmartCallback() {
                    @Override
                    public void result(boolean allPermissionsGranted, boolean somePermissionsDeniedForever){
                    }
                })
                .ask(this);
    }

    private void callIntent(){

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+45 75 39 11 00"));
        startActivity(intent);
    }
}