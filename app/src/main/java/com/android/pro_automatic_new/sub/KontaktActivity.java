package com.android.pro_automatic_new.sub;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.pro_automatic_new.R;

import im.delight.android.webview.AdvancedWebView;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;
import rebus.permissionutils.SmartCallback;

public class KontaktActivity extends AppCompatActivity implements AdvancedWebView.Listener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontakt);

        checkPermission();

        ImageView imv_back = (ImageView) findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(EltavlerActivity.this, MainActivity.class));
                finish();
            }
        });


        TextView txv_tlf = (TextView) findViewById(R.id.txv_tlf);
        txv_tlf.setMovementMethod(LinkMovementMethod.getInstance());
        txv_tlf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callIntent("+45 75 39 11 00");
            }
        });

        TextView txv_fax = (TextView) findViewById(R.id.txv_fax);
        txv_fax.setMovementMethod(LinkMovementMethod.getInstance());
        txv_fax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callIntent("+45 75 39 12 77");
            }
        });

        TextView txv_hovedmail = (TextView) findViewById(R.id.txv_hovedmail);
        txv_hovedmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailIntent("pro@pro-automatic.dk");
            }
        });


        TextView txv_tilbud = (TextView) findViewById(R.id.txv_tilbud);
        txv_tilbud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailIntent("tilbud@pro-automatic.dk");
            }
        });

        TextView txv_bestilling = (TextView) findViewById(R.id.txv_bestilling);
        txv_bestilling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailIntent("ordre@pro-automatic.dk");
            }
        });

        TextView txv_ordrebek = (TextView) findViewById(R.id.txv_ordrebek);
        txv_ordrebek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailIntent("purchase@pro-automatic.dk");
            }
        });

        TextView txv_faktura = (TextView) findViewById(R.id.txv_faktura);
        txv_faktura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailIntent("kreditor@pro-automatic.dk");
            }
        });

        //hyperlink
        TextView txv_kontaktpersoner = (TextView) findViewById(R.id.txv_kontaktpersoner);
        txv_kontaktpersoner.setMovementMethod(LinkMovementMethod.getInstance());

    }

    private void checkPermission() {

        PermissionManager.Builder()
                .permission(PermissionEnum.CALL_PHONE)
                .askAgain(false)
                .callback(new SmartCallback() {
                    @Override
                    public void result(boolean allPermissionsGranted, boolean somePermissionsDeniedForever){
                    }
                })
                .ask(this);
    }

    private void callIntent(String s){

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + s));
        startActivity(intent);
    }

    private void emailIntent(String s){

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{s});
        startActivity(Intent.createChooser(intent, "Send Email"));
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {

    }

    @Override
    public void onPageFinished(String url) {

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
}