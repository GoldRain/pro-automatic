package com.android.pro_automatic_new.sub;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.pro_automatic_new.R;


public class EltavlerActivity extends AppCompatActivity {

    ImageView imv_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eltavler);

        imv_back = (ImageView) findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(EltavlerActivity.this, MainActivity.class));
                finish();
            }
        });
    }
}