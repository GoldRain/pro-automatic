package com.android.pro_automatic_new.sub;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.pro_automatic_new.R;

public class ProfilActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        ImageView imv_back = (ImageView) findViewById(R.id.imv_back);
        imv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(EltavlerActivity.this, MainActivity.class));
                finish();
            }
        });

        TextView txv_profil_hyper = (TextView) findViewById(R.id.txv_profil_hyper);
        txv_profil_hyper.setMovementMethod(LinkMovementMethod.getInstance());
    }
}